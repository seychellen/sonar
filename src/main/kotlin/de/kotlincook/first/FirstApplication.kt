package de.kotlincook.sonar

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SonarApplication

fun main(args: Array<String>) {
    runApplication<SonarApplication>(*args)
}
